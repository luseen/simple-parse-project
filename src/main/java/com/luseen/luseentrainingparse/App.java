package com.luseen.luseentrainingparse;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Created by Chatikyan on 17.12.2016.
 */

public class App extends Application {

    private static final String APPLICATION_ID = "sdfYna7P23ts425798fsadasdfhidqj25fsgmmNasdiadslgbjoif743y";

    private static final String SERVER_URL = "https://apps.luseen.co.uk:1341/api";

    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(Android.class);

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(APPLICATION_ID)
                .server(SERVER_URL)
                .build());
    }
}
