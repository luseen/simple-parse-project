package com.luseen.luseentrainingparse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Chatikyan on 17.12.2016.
 */

@ParseClassName("Android")
public class Android extends ParseObject {

    private boolean isChecked;

    public String getVersionName() {
        return getString("name");
    }

    public int getVersion() {
        return getInt("versions");
    }

    public int getApiLevel() {
        return getInt("api_level");
    }

    public boolean isMaterialDesign(){
        return getBoolean("is_material_design");
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
