package com.luseen.luseentrainingparse;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chatikyan on 18.12.2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Android> androidList = new ArrayList<>();

    public RecyclerViewAdapter(List<Android> androidList) {
        this.androidList = androidList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Android android = androidList.get(position);
        holder.versionName.setText(android.getVersionName());
        holder.version.setText(String.valueOf(android.getVersion()));
        holder.apiLevel.setText(String.valueOf(android.getApiLevel()));
        holder.isMaterialDesign.setText(String.valueOf(android.isMaterialDesign()));

        if (!android.isChecked()) {
            holder.imageView.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        } else {
            holder.imageView.setImageResource(R.drawable.ic_favorite_black_24dp);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.isChecked()) {
                    holder.imageView.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    android.setChecked(false);

                } else {
                    holder.imageView.setImageResource(R.drawable.ic_favorite_black_24dp);
                    android.setChecked(true);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return androidList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cards;
        ImageView imageView;
        TextView versionName;
        TextView version;
        TextView apiLevel;
        TextView isMaterialDesign;

        public ViewHolder(View itemView) {
            super(itemView);
            versionName = (TextView) itemView.findViewById(R.id.version_name);
            version = (TextView) itemView.findViewById(R.id.version);
            apiLevel = (TextView) itemView.findViewById(R.id.api_level);
            isMaterialDesign = (TextView) itemView.findViewById(R.id.is_material_design);
            cards = (CardView) itemView.findViewById(R.id.card_view);
            imageView = (ImageView) itemView.findViewById(R.id.favorite);

            cards.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), androidList
                            .get(getAdapterPosition()).getVersionName(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
